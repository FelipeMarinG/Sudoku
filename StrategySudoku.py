from abc import  abstractmethod
from sudoku import Cell, Board
from Solve import Solver
import random

class StrategySudoku(object):

	@abstractmethod
	def generate(self,*args):
		pass

	def Create(self,N):
		a = Board()
		pos = random.choice(random.choice(a.cells))
		n = 0
		while n < N:
			pos = random.choice(random.choice(a.cells))
			if not pos.is_solved and len(pos.values) > 0:
				pos.set_value(pos.values[0])
				n += 1
		return a
					


class SudokuEasy(StrategySudoku):

	def generate(self):

		EncontreSudoku = False
		while not EncontreSudoku:

			a = self.Create(60)
			b = a.copy()
			sol = Solver(a)
			EncontreSudoku = sol.solve()
		print(b)


class SudokuMedium(StrategySudoku):

	def generate(self):

		EncontreSudoku = False
		while not EncontreSudoku:

			a = self.Create(40)
			b = a.copy()
			sol = Solver(a)
			EncontreSudoku = sol.solve()
		print(b)


class SudokuHard(StrategySudoku):

	def generate(self):

		EncontreSudoku = False
		while not EncontreSudoku:

			a = self.Create(20)
			b = a.copy()
			sol = Solver(a)
			EncontreSudoku = sol.solve()
		print(b)

