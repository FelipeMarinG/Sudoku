JOHAN FELIPE MARIN GONZALEZ

Solucion Sudoku

use el codigo trabajado en clase solamente que cuando cada casilla queda con un posible candidato no se lo ponga como solucion de la casilla

añadi a la Board la opcion para que me diga si esta resuelta, si tiene celdas libres y una lista con la posicion x,y de la casilla con menor numero de candidatos

cree un solver, que usa una pila para simular la recursividad el ingresa una copia de la Board como se la pasen en una pila, luego procede a mirar si el tope de la pila(un Board) esta resuelto, si no si tiene casillas libres si no, que busque la casilla con menores candidatos y cree tantas copias como candidatos tenga luego que en la casilla de cada copia le haga un set del valor,( a cada copia con cada uno de los candidatos) y que repita el proceso hasta que encuentre una solucion

PARA EJECUTAR LA SOLUCION DE SUDOKU CLASICO:
	python3 sudoku.py

para resolver el sudoky serpenteante herede de board y de cell y hice que cada celda guardara que caja le corresponde y a la hora de validar los observadores de la celda, ya ellas miran si estan en la misma con un atributo interno de clase y cuando creo la board tengo que pasarle como atributo al inicializador un string de como se forman las cajas internas del sudoku

los otros metodos funcionan igual, y se soluciona igual que un sudoky normal


PARA EJECUTAR LA SOLUCION DE SUDOKU SERPENTEANTE:
	python3 SudokuRect.py


para generar sudoku's se hizo uso del patron strategia, se creo una clase que guarda las estrategias y las ejecuta, se creo la clase estrategia en StrategySudoku.py que tiene el metodo abstracto generate y un metodo Create que lo que hace es crear una board con N elementos acomodados a lo largo de este sudoku,

hay tres clases para cada tipo de sudoku(facil, medio, dificil) lo que hacen los metodos generar de estas clases que heredan de StrategySudoku
es crear un Board con N elementos asignados, con el solve, validar si se soluciona o no y si se soluciona imprimir la copia que esta sin solucionar, si no se soluciona genere hasta que lo haga


PARA EJECUTAR El GENERADOR DE SUDOKU'S:
	python3 BuildSudoku.py
	
