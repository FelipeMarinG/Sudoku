from StrategySudoku import SudokuEasy, SudokuMedium ,SudokuHard

class Generator(object):

	def __init__(self):

		self.strategy = None

	def setStrategy(self, obj):

		self.strategy = obj

	def getStrategy(self):

		return self.strategy

	#corre la estrategia que neecesita 
	def generateStrategy(self):

		self.strategy.generate()


if __name__=="__main__":
	g = Generator()
	k = Generator()
	z = Generator()

	a = SudokuEasy()
	b = SudokuMedium()
	c = SudokuHard()

	g.setStrategy(a)
	k.setStrategy(b)
	z.setStrategy(c)

	g.generateStrategy()
	k.generateStrategy()
	z.generateStrategy()



