from util  import Observable
from wrapt import synchronized

#from sudoku import Cell
#from sudoku import Board


class Solver():

	def __init__(self, board):
		
		self.board = board

	def solve(self):

		pila = [self.board.copy()]

		while len(pila) > 0:

			validacion = pila[len(pila) - 1].IsSolve()
			if validacion:
				self.board = pila[len(pila) - 1]
				return True

			#validacion = not pila[len(pila) - 1].HasSolution()
			elif not pila[len(pila) - 1].HasSolution():
				pila.pop()

			else:
				#ingresamos todos los posibles caminos para una solucion
				X = pila[len(pila) - 1]
				pila.pop()
				pos = X.Mincandi()
				for candidato in X.cells[pos[0]][pos[1]]:
					Y = X.copy()
					Y.set_value(pos[0], pos[1] , candidato)
					pila.append(Y)
		return False

	def pprint(self):

		print(self.board)





