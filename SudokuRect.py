from util  import Observable
from wrapt import synchronized
import math
import copy
from sudoku import Cell, Board
from Solve import Solver


class CellR(Cell):

	def add_observer(self, cells):
		N = len(cells)
		for i in range(N):
			for j in range(N):
				if i == self.row and j == self.col: continue
				is_sline = (i == self.row) or (j == self.col)
				is_sbox  = cells[i][j].Box == self.Box
				if is_sline or is_sbox:
					#if self.row == self.col == 5:
						#print(self.row, self.col, i, j)
					super(Cell, self).add_observer(cells[i][j])
	

class BoardRec(Board):

	def __init__(self, Cajas,N = 9):

		self.N = 9
		self.Cajas = self.ObtainBox(Cajas, self.N)
		#print (self.Cajas)
		self.cells = [[CellR(i,j , self.Cajas[i][j], self.N) for j in range(N)] for i in range(N)]
		
		# Adiciona observadores
		for i in range(N):
			for j in range(N):
				self.cells[i][j].add_observer(self.cells)

	def ObtainBox(self, txt, N):

		l = [[None for _ in range(N)] for _ in range(N)]
		for i in range(N):
			for j in range(N):
				c = txt[i*N+j]
				
				l[i][j] = int(c)
		return l

	def copy(self):
		return copy.deepcopy(self)

		
if __name__=="__main__":

	fd = open('sudokurec.txt')
	p = fd.readline()
	pp = fd.readline()
	c = BoardRec(p)
	c.setup(pp)
	print(c)
	sol = Solver(c)

	k = sol.solve()
	if k:
		sol.pprint()