#-*- encoding: utf-8-*-
from util  import Observable
from wrapt import synchronized
import math
import copy
from Solve import Solver

class Cell(Observable):

	def __init__(self, row, col , box, N):
		super(Cell, self).__init__()
		self.values = [i+1 for i in range(N)]
		self.is_solved = False
		self.row = row
		self.col = col
		#cual es el tamaño de las cajas dentro del sudoku
		self.Box = box  
		
	def __iter__(self):
		for val in self.values:
			yield val
			
	def set_value(self, value):
		self.values = [value]
		self.is_solved = True		
		self.notify_observers(value)

	def hasCandidate(self):
		return len(self.values) > 0 and not self.is_solved

	def get_value(self):
		#compar = self.is_solved #and len(self.values) > 0
		return self.values[0] if self.is_solved  else 0

	def add_observer(self, cells):
		N = len(cells)
		for i in range(N):
			for j in range(N):
				if i == self.row and j == self.col: continue
				is_sline = (i == self.row) or (j == self.col)
				is_sbox  = (i//self.Box == self.row//self.Box) and (j//self.Box == self.col//self.Box)
				if is_sline or is_sbox:
					#if self.row == self.col == 5:
						#print(self.row, self.col, i, j)
					super(Cell, self).add_observer(cells[i][j])
	
	@synchronized								
	def update(self, value):
		if self.is_solved or value == 0:
			#if self.values[0] == value:
				#self.values = []
				#self.is_solved = False
			#else:
			return

		#if self.row == 5 and self.col == 5:
		#	print(value, self.values)
		if value in self.values: self.values.remove(value)
		#if not self.is_solved and len(self.values) == 1:
		#	self.set_value(self.values[0])
			#print('{} {}'.format(value, self.values[0]))

'''
Representa el tablero del Sudoku
'''
class Board(object):

	def __init__(self, N = 9, B = 3):
		self.cells = [[Cell(i,j , B, N) for j in range(N)] for i in range(N)]
		self.Box = B  #el tamaño de la caja a la que pertenece
		# Adiciona observadores
		for i in range(N):
			for j in range(N):
				self.cells[i][j].add_observer(self.cells)
				
	def __str__(self):
		N = len(self)
		
		str = '+---------+---------+---------+\n'
		for i in range(N):
			str += '|'
			for j in range(N):
				value = self.cells[i][j].get_value()
				str += ' {} '.format(value if value != 0 else '.')
				if (j + 1) % 3 == 0:
					str += '|'
			str += '\n'
			if (i + 1) % 3 == 0:
				str += '+---------+---------+---------+\n'
		str += '\n' + self.candidates()

		return str
		
	def __len__(self):
		return len(self.cells)
		
	def setup(self, puzzle):
		N = len(self)
		if isinstance(puzzle, list):
			self._setup_from_list(puzzle)
		elif isinstance(puzzle, str):
			self._setup_from_str(puzzle)

	def _setup_from_list(self, puzzle):
		N = len(self)
		assert len(puzzle) == N
		for i in range(N):
			for j in range(N):
				if puzzle[i][j] == 0: continue
				self.cells[i][j].set_value(puzzle[i][j])


	def _setup_from_str(self, puzzle):
		N = len(self)
		#assert math.sqrt(len(puzzle)) == N		
		for i in range(N):
			for j in range(N):
				c = puzzle[i*N+j]
				if c == '.': continue
				self.cells[i][j].set_value(int(c))
					
	def set_value(self, row, col, value):

		self.cells[row][col].set_value(value)
	
	def candidates(self):
		N = len(self)		
		str = ''
		for i in range(N):
			for j in range(N):
				str += 'cells[{}][{}] : '.format(i, j)
				for k in self.cells[i][j]:
					str += '{} '.format(k)
				str += '\n'
		return str
	

	def copy(self):
		N = len(self)
		l = []
		for i in range(N):
			l.append([])
			for j in range(N):
				l[i].append(None)

		for row in range(N):
			for col in range(N):
				if self.cells[row][col].is_solved:
					l[row][col] = self.cells[row][col].values[0]
				else:
					l[row][col] = 0
		B = Board()
		B.setup(l)
		return B

	def IsSolve(self):

		N = len(self)

		#valido cajas
		for i in range(N):
			for j in range(N):
				if not self.cells[i][j].is_solved:
					return False
		return True

	def HasSolution(self):

		N = len(self)
		for i in range(N):
			for j in range(N):
				if len(self.cells[i][j].values) == 0:
					return False
		return True 




	def Mincandi(self): 

		#retorna la pos de la celda que tiene menores candidatos  en un lista

		N = len(self)
		l = [] 
		cand = 10
		for row in range(N):
			for col in range(N):
				cel = self.cells[row][col]
				if cel.get_value() == 0:
					if len(cel.values) < cand:
						l = [row,col]
						cand = len(cel.values)
		return l








		



if __name__=="__main__":

	fd = open('sudoku.txt')
	fd.readline()
	fd.readline()
	p = fd.readline()
	fd.readline()

	b = Board()
	b.setup(p)
	print(b)
	sol = Solver(b)

	k = sol.solve()
	if k:
		sol.pprint()



